#!/bin/bash
# Set Port in Minecraft server.properties file.

portRangeMin=${1:-25000}
portRangeMax=${2:-25500}

getNewPort () {
    newPort=$(awk -v min=${portRangeMin} -v max=${portRangeMax} 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
    # echo "New port is: ${newPort}"
    portIsInUse=$(netstat -an | grep $newPort)
}

setNewPort () {
    # echo "$newPort not in use"
    sed -i '' -e "s/.*server-port.*/server-port=$newPort/" ./server.properties
    sed -i '' -e "s/.*query.port.*/query.port=$newPort/" ./server.properties
}

getNewPort

while : ; do
    if [ -z $portIsInUse ]; then
        setNewPort
        break
    fi
    getNewPort
done